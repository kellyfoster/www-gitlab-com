---
layout: markdown_page
title: University | Support Path
---

**This page has been moved to the new [GitLab University support path](https://docs.gitlab.com/ee/university/support/) page**
