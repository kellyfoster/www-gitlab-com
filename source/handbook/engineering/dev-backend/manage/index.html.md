---
layout: markdown_page
title: "Manage Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Manage Team
{: #manage}

The responsibilities of this team are described by the [Manage product
category](/handbook/product/categories/#dev).
